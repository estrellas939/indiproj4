# Rust AWS Lambda and Step Functions
This project is composed of three independent functions, which are linked through step functions to form a complete pipeline.
The function of these Rust file is to get the numbers in the text, then convert them into an array, and finally find the maximum, minimum, and median values in the array.

- Demo video: ![video](https://gitlab.com/estrellas939/indiproj4/-/raw/main/video1860167474.mp4?ref_type=heads&inline=false)

## Part I. Rust AWS Lambda function

### 1. Create New Rust Project
In the terminal, use `cargo lambda new function1`, `cargo lambda new function2` and `cargo lambda new function3` for three individual functions folder.

### 2. Configuration
For all sub-functions, we add the following dependencies:
```
[dependencies]
lambda_runtime = "0.11.0"
serde = "1"
serde_json = "1.0.115"
tokio = { version = "1", features = ["macros"] }
log = "0.4.21"
simple_logger = "4.3.3"
regex = "1.10.4"
```

### 3. Build Functions
- For function 1, the goal is to extract all the numbers from the text.
```
async fn function_handler(event: LambdaEvent<Value>) -> Result<Value, Error> {
    let (event, _context) = event.into_parts();
    // Assume `text` is the string passed in containing text
    let text = event["text"].as_str().ok_or_else(|| lambda_runtime::Error::from("Expected text".to_string()))?;

    // Extract all numbers using regular expression
    let re = regex::Regex::new(r"\d+").unwrap();
    let numbers: Vec<&str> = re.find_iter(text).map(|m| m.as_str()).collect();

    // Concatenate numbers into a string, separating each number with a space
    let numbers_text = numbers.join(" ");
    println!("number text: {}", numbers_text);
    Ok(json!({ "numbers_text": numbers_text }))
}
```

- For function 2, it will receive the result from function 1 and do reorganization to the result.
```
async fn function_handler(event: LambdaEvent<Value>) -> Result<Value, Error> {
    let (event, _context) = event.into_parts();
    
    // Assume `numbers_text` is passed in a string containing only numbers
    let numbers_text = event["numbers_text"].as_str().ok_or_else(|| Error::from("Expected numbers_text"))?;

    // Convert string to numeric array
    let numbers: Vec<i32> = numbers_text.split_whitespace()
                                        .filter_map(|n| n.parse().ok())
                                        .collect();
    
    // Use tracing to log information
    tracing::info!("array: {:?}", numbers);

    // Return JSON response
    Ok(json!({ "numbers": numbers }))
}
```

- For function 3, it will do the operations to the array and return the result.
```
async fn function_handler(event: LambdaEvent<Value>) -> Result<Value, Error> {
    let (event, _context) = event.into_parts();
    let numbers: Vec<f64> = event["numbers"].as_array().ok_or_else(|| Error::from("Expected numbers"))?
        .iter().map(|x| x.as_f64().unwrap()).collect();
    
    // Sort the numbers to calculate min, max, and median
    let mut sorted_numbers = numbers.clone();
    sorted_numbers.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());
    
    let max = sorted_numbers.last().cloned().ok_or_else(|| Error::from("Array is empty"))?;
    let min = sorted_numbers.first().cloned().ok_or_else(|| Error::from("Array is empty"))?;
    let mid_index = sorted_numbers.len() / 2;
    let mid = if sorted_numbers.len() % 2 == 0 {
        (sorted_numbers[mid_index - 1] + sorted_numbers[mid_index]) / 2.0
    } else {
        sorted_numbers[mid_index]
    };

    Ok(json!({ "max": max, "min": min, "mid": mid }))
}
```

### 4. Local Test
Because lambda function mainly receive JSON files as request, we need to create test file `test.txt` and empty JSON files before the local test.
- `test.txt`
```
{
  "text": "This is a test file for function I. For example, user I has 100 subscriptions. User II has 500. III has 1000."
}
```
In the terminal, run `cargo lambda watch` and run `cargo lambda invoke --data-file test.txt` in another terminal.
- Function 1 output:
```
{
  "numbers_text":"100 500 1000"
}
```

- Function 2 output:
```
{
    "numbers":[100,500,1000]
}
```

- Function 3 output:
```
{
    "max":1000.0,"mid":500.0,"min":100.0
}
```
Now all the functions had passed the local test and ready to deploy to AWS Lambada.

## Part II. Step Functions workflow coordinating Lambdas

### 5. AWS IAM Role
Before deploying to AWS Lambda function, we need to create three IAM roles for each functions. 
![screenshot of iam](https://gitlab.com/estrellas939/indiproj4/-/raw/main/pic/iam.png?inline=false)

### 6. AWS Lambda Deployment
Then, we can run `cargo lambda build --release` and `cargo lambda deploy --iam-role {ROLE ARN}` to deploy to Lambda.
![screenshot of lambda](https://gitlab.com/estrellas939/indiproj4/-/raw/main/pic/lambda.png?inline=false)

### 7. AWS Step Function
Next, we need to create and define a state machine as our step function.
- When setting the state machine, we need to config it as the following so that it can have the whole processing for all three functions.
```
{
  "Comment": "Orchestrates Lambda functions",
  "StartAt": "FirstFunction",
  "States": {
    "FirstFunction": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-1:568937614068:function:week9proj",
      "Next": "SecondFunction"
    },
    "SecondFunction": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-1:568937614068:function:function2",
      "Next": "ThirdFunction"
    },
    "ThirdFunction": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-1:568937614068:function:function3",
      "End": true
    }
  }
}
```

![screenshot of state machine](https://gitlab.com/estrellas939/indiproj4/-/raw/main/pic/statemachine.png?inline=false)

Also, it is import to set necessary permissions to the state machine so that it can work properly.
- For example, we can add `Cloudwatch` so that the state machine can have access to the log file.


## Part III. Orchestrate data processing pipeline

### 8. AWS Step Function Test with Lambda Functions
To test our pipeline, we need to create a execution first. Remeber Lambda functions accept JSON format. We can put `test.txt` content here.

Then, just need to click `Start execution` button. And you should see the following output:
![screenshot of flow chart green](https://gitlab.com/estrellas939/indiproj4/-/raw/main/pic/execution.png?inline=false)

### 9. AWS Step Function CLI Test
We can also test our state machine from the terminal.
- Run `aws stepfunctions start-execution --state-machine-arn arn:aws:states:us-east-1:568937614068:stateMachine:GetMaxMinMid --input file://test.txt` to start a new execution.
- You may see the following result:
```
{
    "executionArn": "arn:aws:states:us-east-1:568937614068:execution:GetMaxMinMid:373451c6-aded-4ea4-af39-9a76fbbb4c64",
    "startDate": "2024-04-04T18:21:36.235000-04:00"
}
```

- Run `aws stepfunctions describe-execution --execution-arn "arn:aws:states:us-east-1:568937614068:execution:GetMaxMinMid:373451c6-aded-4ea4-af39-9a76fbbb4c64"` to display the result.
- You may see the result displayed:
```
{
    "executionArn": "arn:aws:states:us-east-1:568937614068:execution:GetMaxMinMid:373451c6-aded-4ea4-af39-9a76fbbb4c64",
    "stateMachineArn": "arn:aws:states:us-east-1:568937614068:stateMachine:GetMaxMinMid",
    "name": "373451c6-aded-4ea4-af39-9a76fbbb4c64",
    "status": "SUCCEEDED",
    "startDate": "2024-04-04T18:21:36.235000-04:00",
    "stopDate": "2024-04-04T18:21:37.805000-04:00",
    "input": "{\n  \"text\": \"This is a test file for function I. For example, user I has 100 subscriptions. User II has 500. III has 1000.\"\n}\n",
    "inputDetails": {
        "included": true
    },
    "output": "{\"max\":1000.0,\"mid\":500.0,\"min\":100.0}",
    "outputDetails": {
        "included": true
    },
    "redriveCount": 0,
    "redriveStatus": "NOT_REDRIVABLE",
    "redriveStatusReason": "Execution is SUCCEEDED and cannot be redriven"
}
```

Now we have built a complete pipeline for AWS Step Function and Lambda functions.