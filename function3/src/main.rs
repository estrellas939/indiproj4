use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde_json::{json, Value};
use serde::{Deserialize, Serialize};
use std::vec::Vec;
use tracing_subscriber;

#[derive(Deserialize)]
struct Request {
    command: String,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

async fn function_handler(event: LambdaEvent<Value>) -> Result<Value, Error> {
    let (event, _context) = event.into_parts();
    let numbers: Vec<f64> = event["numbers"].as_array().ok_or_else(|| Error::from("Expected numbers"))?
        .iter().map(|x| x.as_f64().unwrap()).collect();
    
    // Sort the numbers to calculate min, max, and median
    let mut sorted_numbers = numbers.clone();
    sorted_numbers.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());
    
    let max = sorted_numbers.last().cloned().ok_or_else(|| Error::from("Array is empty"))?;
    let min = sorted_numbers.first().cloned().ok_or_else(|| Error::from("Array is empty"))?;
    let mid_index = sorted_numbers.len() / 2;
    let mid = if sorted_numbers.len() % 2 == 0 {
        (sorted_numbers[mid_index - 1] + sorted_numbers[mid_index]) / 2.0
    } else {
        sorted_numbers[mid_index]
    };

    Ok(json!({ "max": max, "min": min, "mid": mid }))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    
    tracing_subscriber::fmt::init();

    run(service_fn(function_handler)).await
}
