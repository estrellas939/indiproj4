use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde_json::{json, Value};
use serde::{Deserialize, Serialize};
use tracing_subscriber;

#[derive(Deserialize)]
struct Request {
    command: String,
}

#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

async fn function_handler(event: LambdaEvent<Value>) -> Result<Value, Error> {
    let (event, _context) = event.into_parts();
    
    // Assume `numbers_text` is passed in a string containing only numbers
    let numbers_text = event["numbers_text"].as_str().ok_or_else(|| Error::from("Expected numbers_text"))?;

    // Convert string to numeric array
    let numbers: Vec<i32> = numbers_text.split_whitespace()
                                        .filter_map(|n| n.parse().ok())
                                        .collect();
    
    // Use tracing to log information
    tracing::info!("array: {:?}", numbers);

    // Return JSON response
    Ok(json!({ "numbers": numbers }))
}

// main function
#[tokio::main]
async fn main() -> Result<(), Error> {
    // Initialize tracing subscribers
    tracing_subscriber::fmt::init();

    // Run the Lambda function processor
    run(service_fn(function_handler)).await
}
