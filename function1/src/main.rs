
use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use serde_json::{Value, json};

/// This is a made-up example. Requests come into the runtime as unicode
/// strings in json format, which can map to any structure that implements `serde::Deserialize`
/// The runtime pays no attention to the contents of the request payload.
#[derive(Deserialize)]
struct Request {
    command: String,
}

/// This is a made-up example of what a response structure may look like.
/// There is no restriction on what it can be. The runtime requires responses
/// to be serialized into json. The runtime pays no attention
/// to the contents of the response payload.
#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
/// - https://github.com/aws-samples/serverless-rust-demo/
async fn function_handler(event: LambdaEvent<Value>) -> Result<Value, Error> {
    let (event, _context) = event.into_parts();
    // Assume `text` is the string passed in containing text
    let text = event["text"].as_str().ok_or_else(|| lambda_runtime::Error::from("Expected text".to_string()))?;

    // Extract all numbers using regular expression
    let re = regex::Regex::new(r"\d+").unwrap();
    let numbers: Vec<&str> = re.find_iter(text).map(|m| m.as_str()).collect();

    // Concatenate numbers into a string, separating each number with a space
    let numbers_text = numbers.join(" ");
    println!("number text: {}", numbers_text);
    Ok(json!({ "numbers_text": numbers_text }))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
